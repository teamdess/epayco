import { AlertPlugin } from "bootstrap-vue"

 
  export const mutations = {
    SET_LANG (state, locale) {
      if (state.locales.indexOf(locale) !== -1) {
        state.locale = locale
      }
    }
  }

  export const getters = {
    isAuthenticated(state) {
      return state.auth.loggedIn
    },
  
    loggedInUser(state) {
      return state.auth.user
    }
  }

  export const state = () => ({
    locales: ['en', 'es'],
    locale: 'es'
  })