import env from '../config/env'

export const state = () => ({
  datos: [],
  cliente: {},
});

export const mutations = {
    set(state, cliente) {
        state.datos = cliente
      },
    mergeCliente(state, form) {
        assign(state.cliente, form)
      },
};

export const actions = {
    async get({commit}) {
      
        await this.$axios.get(`${env.endpoint}/clientes`)
          .then((res) => {
            if (res.status === 200) {
              commit('set', res.data)
            }
          })
      },
  delete({commit}, params) {
    return this.$axios.delete(`${env.endpoint}/deleteclientes/${params.id}`, {datos: params})
  },
  update({commit}, params) {
      return this.$axios.put(`${env.endpoint}/updateclientes`, {datos: params})
  },
  create({commit}, params) {
    console.log(params);
    return this.$axios.post(`${env.endpoint}/clientes`, {datos: params})
  },
};
