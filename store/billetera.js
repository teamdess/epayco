import env from '../config/env'

export const state = () => ({
  datos: [],
  billetera: {}
});

export const mutations = {
    set(state, billetera) {
        state.datos = billetera
      },
      mergebilletera(state, form) {
        assign(state.billetera, form)
      },
};

export const actions = {
    recargarBilletera({commit}, params) {
      return this.$axios.post(`${env.endpoint}/updatebilletera`, {datos: params});
  },
  consultarsaldoBilletera({commit}, params) {
    return this.$axios.post(`${env.endpoint}/consultarsaldoBilletera`, {datos: params});
}
};
